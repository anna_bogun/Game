﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace Assets.Scripts
{
    public class ViewClues : MonoBehaviour
{
    public Text label;
    // Use this for initialization
    void Start()
    {
        label.text = getClues();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static string getClues()
    {
        int i = 0;
        string clues = ""+ '\n';
        foreach (Clue clue in ClueScript.state.Clues)
        {
            clues += ++i +". " + clue.Name + "("+clue.Description+")"+'\n';
         }
        return clues;
    }
}}
