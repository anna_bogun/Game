﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class ClueScript : MonoBehaviour
    {
        private static Dictionary<string, Clue> clues;
        public static Info state = new Info();
        public Text label;

        public static Info State
        {
            get
            {
                return state;
            }

            set
            {
                state = value;
            }
        }

        void Start()
        {
            clues = new Dictionary<string, Clue>();
            clues.Add("sofa", new Clue("диван", "- Странные пятна... Неужели это кровь?"));
            clues.Add("book", new Clue("дневник", "- Это... Дневник Мэги!!! Она пишет, что ей грустно и одиноко("));
        }
        void Update()
        {

        }

        void OnTriggerEnter2D(Collider2D other)
        {
            foreach (KeyValuePair<string, Clue> entry in clues)
            {
                if (other.gameObject.CompareTag("maggy")) {
                    Application.LoadLevel("dialog");
                    break;
                }
                if (other.gameObject.CompareTag(entry.Key))
                {
                    state.addCLue(entry.Value);
                    label.text = entry.Value.Description;
                    break;
                }
                else
                {
                    label.text = "";
                }
            }
        }

    }
}
