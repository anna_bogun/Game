﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class Clue
    {
        private string name;
        private string description;
        private string person;

        public Clue() { }
        public Clue(string name, string description)
        {
            this.Name = name;
            this.description = description;
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public string Person
        {
            get
            {
                return person;
            }

            set
            {
                person = value;
            }
        }
    }
}
