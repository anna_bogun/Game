﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Assets.Scripts
{
  public class Info
    {
        private HashSet<Clue> clues = new HashSet<Clue>();
        public int level;

        public HashSet<Clue> Clues
        {
            get
            {
                return clues;
            }

            set
            {
                clues = value;
            }
        }

        public void addCLue(Clue clue) {
            clues.Add(clue);
        }
    }
}